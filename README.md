# About project prs (Paper, Rock, Scissors)

The solution is written in the django rest framework, based on websockets - provided by django-channels.

## TODO:

1. Frontend layer that will update the game results and check its status. At the moment, a very simple front-end for testing purposes in terms of the backend layer.

2. Unit tests of a solution written in django-channels.

3. Better logging of information, currently using the `print()` method.
