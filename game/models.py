import uuid as uuid

from django.contrib.auth.models import User
from django.db import models


class Player(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile', unique=True)
    number_of_wins = models.PositiveIntegerField(default=0)

    def __str__(self):
        return str(self.uuid)


class GameSession(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    players = models.ManyToManyField(Player, blank=True)
    is_started = models.BooleanField(default=False)

    def __str__(self):
        return str(self.uuid)
