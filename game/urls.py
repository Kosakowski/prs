from django.urls import path

from game.views import PlayerApiView, RankingPlayerApiView, GameApiView, GameView

app_name = "game"

urlpatterns = [
    path("player/", PlayerApiView.as_view(), name="player"),
    path("ranking-players/", RankingPlayerApiView.as_view(), name="ranking-players"),
    path("game/", GameApiView.as_view(), name="game-api"),
    path("<uuid:game_uuid>/<uuid:user_uuid>", GameView.as_view(), name="game-template"),
]
