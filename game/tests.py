from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory, APIClient

from game.models import Player, GameSession


class PlayerApiTestCase(APITestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.user_1 = User.objects.create(username="user1")
        self.user_2 = User.objects.create(username="user2")
        self.player_1 = Player.objects.create(uuid="4df43233-f8a3-41af-a25b-52246eeb01e9", user=self.user_1, number_of_wins=2)
        self.player_2 = Player.objects.create(uuid="efb84e61-02b3-4447-b4ec-8ec0f0c5b87b", user=self.user_2, number_of_wins=1)
        self.client.force_authenticate(self.user_1)
        self.client.force_authenticate(self.user_2)
        self.urls = {
            'player': reverse("prs:player"),
            'ranking': reverse("prs:ranking-players")
        }

    def test_get_players_list(self):
        response = self.client.get(self.urls["player"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)

    def test_create_player_object(self):
        data = {
            "uuid": "ac11755d-1adb-4964-abf6-8d662bc29529",
            "user": {
                "username": "mariusz",
                "email": "",
                "password": "pass"
            },
            "number_of_wins": 0
        }
        response = self.client.post(self.urls["player"], data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_ranking_players(self):
        response = self.client.get(self.urls["ranking"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['data'][0]["uuid"], "4df43233-f8a3-41af-a25b-52246eeb01e9")
        self.assertEqual(response.data['data'][1]["uuid"], "efb84e61-02b3-4447-b4ec-8ec0f0c5b87b")


class GameSessionApiTestCase(APITestCase):
    def setUp(self) -> None:
        super().setUp()
        self.client = APIClient()
        self.factory = APIRequestFactory()
        self.user_1 = User.objects.create(username="user1")
        self.user_2 = User.objects.create(username="user2")
        self.player_1 = Player.objects.create(uuid="4df43233-f8a3-41af-a25b-52246eeb01e9", user=self.user_1, number_of_wins=2)
        self.player_2 = Player.objects.create(uuid="efb84e61-02b3-4447-b4ec-8ec0f0c5b87b", user=self.user_2, number_of_wins=1)
        self.client.force_authenticate(self.user_1)
        self.client.force_authenticate(self.user_2)
        self.game = GameSession.objects.create(uuid="04a5617d-a5f9-4172-91b7-0d87d4a7228f")
        self.urls = {
            'game-api': reverse("prs:game-api"),
        }

    def test_get_game(self):
        response = self.client.get(self.urls["game-api"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)

    def test_create_game(self):
        data = {
            "uuid": "04a5617d-a5f9-4172-91b7-0d87d4a7228f",
            "is_started": False,
            "players": []
        }
        response = self.client.post(self.urls["game-api"], data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
