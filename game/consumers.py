import json

from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from uuid import UUID

from game.models import GameSession, Player


@sync_to_async
def update_game_status(game_uuid):
    game_obj = GameSession.objects.get(uuid=game_uuid)
    game_obj.is_started = True
    game_obj.save()
    print("Game already started!")
    return game_obj


@sync_to_async
def get_players_count(game_uuid):
    return GameSession.objects.get(uuid=game_uuid).players.count()


@sync_to_async
def is_player_in_game(game_uuid, user_uuid):
    return True if UUID(user_uuid) in GameSession.objects.get(uuid=game_uuid).players.values_list('uuid', flat=True) else False


@sync_to_async
def add_player_to_match(game_uuid, player):
    player_obj = Player.objects.get(uuid=player)
    game_session = GameSession.objects.get(uuid=game_uuid)
    game_session.players.add(player_obj)
    return game_session


class GameConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        game_uuid = self.scope["url_route"]["kwargs"]["game_uuid"]
        self.player = self.scope["url_route"]["kwargs"]["player_uuid"]

        players_count = await get_players_count(game_uuid)

        if not await is_player_in_game(game_uuid, self.player):
            if players_count < 2:
                await add_player_to_match(game_uuid, self.player)
            else:
                print("Too much users. See you in the next game!")

        if await get_players_count(game_uuid) == 2:
            await update_game_status(game_uuid)

        self.game_group_name = f"game_{game_uuid}"
        await self.channel_layer.group_add(self.game_group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(self.game_group_name, self.channel_name)

    async def receive(self, text_data):
        """
        Receive message from WebSocket.
        Get the event and send the appropriate event
        """
        response = json.loads(text_data)
        event = response.get("event", None)
        if event == 'PAPER':
            await self.channel_layer.group_send(self.game_group_name, {
                'type': 'send_message',
                "event": "PAPER"
            })

        if event == 'ROCK':
            await self.channel_layer.group_send(self.game_group_name, {
                'type': 'send_message',
                'event': "ROCK"
            })

        if event == 'SCISSORS':
            await self.channel_layer.group_send(self.game_group_name, {
                'type': 'send_message',
                'event': "SCISSORS"
            })

    async def send_message(self, res):
        await self.send(text_data=json.dumps({
            "payload": res,
        }))
