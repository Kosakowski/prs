from django.urls import re_path

from . import consumers

websocket_urlpatters = [
    re_path(r'ws/game/(?P<game_uuid>[0-9a-f-]+)/(?P<player_uuid>[0-9a-f-]+)', consumers.GameConsumer.as_asgi()),
]
